from django.urls import path
from accounts.views import login_user, log_out_user, show_sign_up_form

urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", log_out_user, name="logout"),
    path("signup/", show_sign_up_form, name="signup"),
]
