from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.


@login_required
def show_all_list_project(request: HttpRequest) -> HttpResponse:
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list_projects.html", context)


# @login_required
# def project_detail(request, id: HttpRequest) -> HttpResponse:
#     project_details = Task.objects.get(id=id)
#     context = {"project_details": project_details}
#     return render(request, "projects/project_detail.html", context)


@login_required
def project_detail(request, id: HttpRequest) -> HttpResponse:
    project_details = get_object_or_404(Project, id=id)
    context = {"project_details": project_details}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
