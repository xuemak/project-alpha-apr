from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from tasks.forms import CreateTaskForm
from tasks.models import Task
from django.http import HttpResponseRedirect


# Create your views here.
@login_required
def create_task(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request: HttpRequest) -> HttpResponse:
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/assigned_tasks.html", context)


@login_required
def edit_task(request, id):
    post = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = CreateTaskForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            next = request.POST.get("next", "/")
            return HttpResponseRedirect(next)
            # return redirect("show_project", id=id)
    else:
        form = CreateTaskForm(instance=post)
    context = {
        "task": post,
        "form": form,
    }
    return render(request, "tasks/edit.html", context)
